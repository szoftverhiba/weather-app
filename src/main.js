import Vue from "vue";
import App from "./App.vue";
import VueSkycons from "vue-skycon";

// or pass the color option
Vue.use(VueSkycons, { color: "white" });

Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");
