import axios from "axios";

const proxy = "https://cors-anywhere.herokuapp.com/",
  API_KEY = "3ee1b3562a595234b099bd02851f6adf",
  API_URL = `${proxy}https://api.darksky.net/forecast/${API_KEY}/`,
  ADDRESS_URL = `${proxy}https://darksky.net/rgeo?`;

function getPosition() {
  return new Promise((res, rej) => {
    navigator.geolocation.getCurrentPosition(res, rej);
  });
}

async function getWeather() {
  const position = await getPosition();
  let lat, lon;

  if (position) {
    (lat = position.coords.latitude), (lon = position.coords.longitude);
  } else {
    lat = 47.4984;
    lon = 19.0405;
  }
  const data = await axios(`${API_URL}${lat},${lon}?units=si`);
  const address = await axios(`${ADDRESS_URL}lat=${lat}&lon=${lon}`);

  return {
    weather: data.data,
    address: address.data.name
  };
}

export default { getWeather };
